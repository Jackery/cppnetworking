// CNetworrkingApp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

#define ASIO_STANDALONE
#include <asio.hpp>
#include <asio/ts/buffer.hpp>
#include <asio/ts/internet.hpp>
#include <chrono>

std::vector<char> vbuffer(1024 * 20);

void grabData(asio::ip::tcp::socket& socket) 
{
	socket.async_read_some(asio::buffer(vbuffer.data(), vbuffer.size()), 
		[&](std::error_code ec, std::size_t length)
		{

			if (!ec)
			{
				std::cout << "Reading " << length << " bytes " << std::endl;

				for (int i = 0; i < length; i++) {
					std::cout << vbuffer[i];
				}

				grabData(socket);
			}

		});
}

int main()
{
	// IO context
	asio::io_context context;

	// Error code for ASIO errors
	asio::error_code ec;

	asio::io_context::work idleWork(context);

	// Thread to run the IO context
	std::thread thread = std::thread([&]() { context.run(); });

	// IP address
	asio::ip::address addr = asio::ip::make_address("93.184.216.34", ec);

	// TCP endpoint
	asio::ip::tcp::endpoint endpoint(addr, 80);

	// Raw TCP socket
	asio::ip::tcp::socket socket(context);

	try
	{
		socket.connect(endpoint);
	}
	catch (std::system_error & ex)
	{
		std::cout << ex.what() << std::endl;
	}

	if (socket.is_open())
	{
		grabData(socket);

		std::cout << "It is open!" << std::endl;

		std::string req =
			"GET /index.html HTTP/1.1\r\n"
			"Host: example.com\r\n"
			"Connection: close\r\n\r\n";

		// Write the HTTP request to the socket
		socket.write_some(asio::buffer(req.data(), req.size()), ec);

		using namespace std::chrono_literals;
		std::this_thread::sleep_for(2000ms);

		context.stop();

		if (thread.joinable())
		{
			thread.join();
		}
	}

	std::cout << "Hello World!\n" << ec.message();
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
